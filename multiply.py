import random
def md(n):
    p = random.random()
    #print p
    d = 0
    if p <= 1.0/n:
        d = 1
    else:
        start = 1.0/n
        for i in range(2,n+1):
            end = start + 1.0/(i*(i-1))
            #print start, end
            if p <= end:
                d = i
                break
            start = end
    return d
def multiply(t):
    if t == 'i':
        m = 12000+md(10)*50+md(10)*5+md(100)*100
        m /= float(10000)
        if m > 1.5:
            m = 1.2 + (0.05 *m)
    elif t == 'r':
        m = 10200+md(10)*50+md(10)*5+md(100)*100
        m /= float(10000)
        if m > 1.2:
            m = 1.02 + (0.05 *m)
    elif t == 'c':
        m = 10800+md(10)*50+md(10)*5+md(100)*100
        m /= float(10000)
        if m > 1.2:
            m = 1.1 + (0.05 *m)
    return m

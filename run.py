# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'clickMe.ui'
#
# Created: Tue Jun 24 13:41:43 2014
#      by: PyQt4 UI code generator 4.11
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(400, 300)
        self.lt = QtGui.QPushButton(Dialog)
        self.lt.setGeometry(QtCore.QRect(40, 50, 101, 23))
        self.lt.setObjectName(_fromUtf8("lt"))
        self.rlf = QtGui.QPushButton(Dialog)
        self.rlf.setGeometry(QtCore.QRect(230, 50, 75, 23))
        self.rlf.setObjectName(_fromUtf8("rlf"))

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "Dialog", None))
        self.lt.setText(_translate("Dialog", "Luby Transform", None))
        self.rlf.setText(_translate("Dialog", "RLF", None))


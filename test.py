import struct, math, random, time, copy
from collections import defaultdict

def xor(A,B):
	out = ""
	for(a,b) in zip(A,B):
		out += chr(ord(a) ^ ord(b))
	return out
a = "abc"
b = "000"
wynik = xor(a,b)
#print wynik
#print xor(wynik,b) 

key = 0L
print key

key = (1<<1)
print key
key = (1<<2)
print key
key |= (1<<1)
key |= (1<<2)
print key

import matplotlib.pyplot as plt
def showHist(x,bins):
    plt.hist(x, bins, color='blue')
    plt.show()

import struct

def decode(data):
	header = "<QQQ"
	hlen = struct.calcsize(header)
	(k,n,d) = struct.unpack_from(header,data)
	value = data[hlen:]
	keys = set()
	for i in range (16):
		if k & (1<<i) != 0:
			keys.add(i)
	print d
	print n
	return keys
	
data = open('strumien','rb').read()

print decode(data)

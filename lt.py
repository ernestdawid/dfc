import random
import struct
import binascii
import math
import ast
from bec import BEC
from hist import showHist
from run import Ui_Dialog
from multiply import multiply
from gui_lt import Ui_Luby_Transform
from gui_rlf import Ui_Random_Linear_Fountain_Code
from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import *
from PyQt4.QtCore import *


def xor( A, B ):
    wynik = ""
    for (a,b) in zip( A, B ):
                wynik += chr( ord(a) ^ ord(b) )
    return wynik
    
def isd(n): #random function with 'ideal soliton distribution' (ISD)
    p = random.random()
    #print p
    d = 0
    if p <= 1.0/n:
        d = 1
    else:
        start = 1.0/n
        for i in range(2,n+1):
            end = start + 1.0/(i*(i-1))
            #print start, end
            if p <= end:
                d = i
                break
            start = end
    return d

def rsd(n,c,delta):
    p = random.random()
    S = math.ceil( c * math.log(n/delta) * math.sqrt(n) )
    g = round(n/S)
    p = random.random()
    d = 0
    norm = 0
    dist = {}
    dist[1] = 1.0/n
    for i in range(2,n+1):
        dist[i] = 1.0/(i*(i-1))
    norm = sum(dist.values())
    #print norm
    for i in range(1,n+1):
        if i < g:
            dist[i] += S/(n*i)
        elif i == g:
            dist[i] += (S * math.log(S/delta) )/n

    norm = sum(dist.values())
    #print 'sum', norm
    for sample, i in enumerate(dist):
        dist[i] /= norm
    #print dist
    #print 'suma', sum(dist.values())
    d = 0
    if p <= dist[1]:
        d = 1
    else:
        start = dist[1]
        for i in range(2,n+1):
            end = start + dist[i]
            #print start, end
            if p <= end:
                d = i
                break
            start = end
    return d
def userDefinedDistribution(path):
    fname = open(path,'r')
    data = fname.read()
    fname.close()
    dist = ast.literal_eval(data)
    
    norm = sum(dist.values())
    #print 'sum', norm
    for sample, i in enumerate(dist):
        dist[i] /= float(norm)
    #print dist
    #print 'suma', sum(dist.values())
    p = random.random()
    d = 0
    if p <= dist[1]:
        d = 1
    else:
        start = dist[1]
        for i in range(2,len(dist)):
            end = start + dist[i]
            #print start, end
            if p <= end:
                d = i
                break
            start = end
    return d
    
#print rsd(100,0.3,0.05)

def split(data, n):
    parts = []
    part_len = (len(data) - (len(data) % n)) / n + 1
    #print 'dlugosc paczki danych', part_len
    for i in range(n):
        index = i * part_len
        part = data[index : index + part_len]
        Q = part_len - len(part)
        part += chr(0)*Q
        parts.append(part)
    
    return parts
    
class chunk(object):
    def __init__(self, data, key):
        self.data = data
        self.key = key

def encode(data, n, distType, c, delta):
    parts = split(data,n)
    part = ""
    opart = ""
    #print parts
    if distType == 'm':#rlf
        d = random.randint(1,n) 
    elif distType == 'i': #lt ISD
        d = isd(n)
    elif distType == 'r': #lt RSD
        d = rsd(n,c,delta)
    elif not distType:
        d = 1
    else:
        d = userDefinedDistribution(distType)
    sparts = set()
    #print 'encode: stopien xorowania', d
    random_set = set()
    random_set = random.sample(range(n), d)
    for r in random_set:
        #print 'r',r
        sparts.add(parts[r])
    for i, element in enumerate(sparts):
        if d == 1:
                return element
        elif i == 0:
            #print 'element', element
            part = element
            #print 'part',part, 'i', i
        else:
                part = xor(part,element)
                #print 'part2',part
    return part
    
def decode(data, n, distType, c, delta):
    if distType == 'm':
        d = random.randint(1,n)
    elif distType == 'i':
        d = isd(n)
    elif distType == 'r':
        d = rsd(n,c,delta)
    elif not distType:
        d = 1
    else:
        d = userDefinedDistribution(distType)
    #print 'decoded d =', d
    value = data
    keys = set()
    keys = random.sample(range(n), d)
    #print value
    return chunk(value, keys)
    
def decodeMessage(inputData, n, pBEC, distType, c, delta, backParam):
    e = backParam['e']
    dict_index = 0
    parts = backParam['parts']
    message = backParam['message']
    current_one_degree = []
    multiply = backParam['multiply']
    j = 0
    send_count = backParam['send_count']
    while send_count < n * multiply and len(message) < n:
        send_count +=1
        seedBEC = random.randint(0,2**28)
        seed = random.randint(0,2**30)
        
        random.seed(seed)
        data = encode(inputData, n, distType, c, delta)
        random.seed(seedBEC)
        dataFromBEC = BEC(data,pBEC)
        random.seed(seed)
        
        if dataFromBEC:
            decodedChunk = decode(dataFromBEC,n, distType, c, delta)
            if len (decodedChunk.key ) != 1:
                chunk_index = set()
                for i in message:
                    for key in decodedChunk.key:
                        if key == i:
                            chunk_index.add(i)
                #print 'success second part', chunk_index
                for i in chunk_index:
                    #print 'you look for this', decodedChunk.key, 'data', decodedChunk.data
                    decodedChunk.data = xor(decodedChunk.data, message[i])
                    decodedChunk.key.remove(i)
                if len (decodedChunk.key) > 1:
                    #print 'success len > 1', decodedChunk.key
                    parts[dict_index] = decodedChunk
                elif len (decodedChunk.key) == 1:
                    #print 'success len == 1', decodedChunk.key, 'data' ,decodedChunk.data
                    for key in decodedChunk.key:
                        message[key] = decodedChunk.data
                        #print key
                """parts[dict_index] = decodedChunk
                dict_index+=1"""
                
            else:
                #print 'wiadomosc', decodedChunk.data
                for i in decodedChunk.key:
                    indice = i
                    value = decodedChunk.data
                    #print indice
                chunk_index = set()
                for i in parts:
                    for key in parts[i].key:
                        if key == indice:
                            #print "success, it works"
                            chunk_index.add(i)
                for i in chunk_index:
                    temp_value = parts[i].data
                    parts[i].data = xor(temp_value, value)
                    parts[i].key.remove(indice)
                    #print 'xoring, data = ', parts[i].data, 'indices =', parts[i].key
                    if len(parts[i].key) == 1:
                        current_one_degree.append(i)
                message[indice] = value
                for i in current_one_degree:
                    for key in parts[i].key:
                        message[key] = parts[i].data
                    del parts[i]
                    #print 'current', current_one_degree

                del current_one_degree
                current_one_degree = []
        else:
            e+=1
            #print 'e', e, 'send_count', send_count, 'pBEC', pBEC
    #print message
    #print
    """print 'probek', send_count
    print 'wymazanych probek', e
    print 'odebranych probek', send_count - e"""
    backParam = {'message': message, 'parts': parts, 'send_count': send_count, 'e': e, 'multiply': multiply}
    return {'wiadomosc': message, 'ilosc':send_count, 'usuniete': e, 'backParam': backParam}

class MyDialog(QtGui.QDialog):
    def __init__(self, parent=None):
        super(MyDialog, self).__init__(parent)
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        QtCore.QObject.connect(self.ui.lt,QtCore.SIGNAL("clicked()"), self.on_pushButton_clicked)
        self.dialogTextBrowser = ltDialog(self)
        QtCore.QObject.connect(self.ui.rlf,QtCore.SIGNAL("clicked()"), self.on_pushButton_clicked1)
        self.dialogTextBrowser1 = rlfDialog(self)

    @QtCore.pyqtSlot()
    def on_pushButton_clicked(self):
        self.dialogTextBrowser.exec_()

    @QtCore.pyqtSlot()
    def on_pushButton_clicked1(self):
        self.dialogTextBrowser1.exec_()
        
class ltDialog(QtGui.QDialog):
    message = {}
    parts = {}
    send_count = 0
    e = 0
    multiply = 1.0
    path = ''
    distType = 'i'
    def __init__(self, parent=None):
        super(ltDialog, self).__init__(parent)
        self.ui = Ui_Luby_Transform()
        self.ui.setupUi(self)
        self.ui.inputFileButton.setEnabled(False)
        self.ui.resendButton.setEnabled(False)
        QtCore.QObject.connect(self.ui.encodeButton,QtCore.SIGNAL("clicked()"), self.flowData)
        QtCore.QObject.connect(self.ui.clearButton,QtCore.SIGNAL("clicked()"), self.clearData)
        QtCore.QObject.connect(self.ui.resendButton,QtCore.SIGNAL("clicked()"), self.resendData)
        QtCore.QObject.connect(self.ui.inputFileButton,QtCore.SIGNAL("clicked()"), self.selectFile)
        QtCore.QObject.connect(self.ui.rsdRadioButton,QtCore.SIGNAL("clicked()"), self.rsdParam)
        QtCore.QObject.connect(self.ui.isdRadioButton,QtCore.SIGNAL("clicked()"), self.isdParam)
        QtCore.QObject.connect(self.ui.userDistRadioButton,QtCore.SIGNAL("clicked()"), self.uddParam)
        QtCore.QObject.connect(self.ui.histButton,QtCore.SIGNAL("clicked()"), self.generateHist)

    def flowData(self):
        data = str(self.ui.textEncode.toPlainText())
        n = self.ui.packetLength.value()
        c = self.ui.cParam.value()
        delta = self.ui.deltaParam.value()
        pBEC = self.ui.becParam.value()
        backParam = {'message': self.message, 'parts': self.parts, 'send_count': self.send_count, 'e': self.e, 'multiply': self.multiply}
        odpowiedz = decodeMessage(data,n, pBEC, self.distType, c, delta, backParam)
        backParam = odpowiedz['backParam']
        message_new = odpowiedz['wiadomosc']
        recived_num = odpowiedz['ilosc']
        erased_num = odpowiedz['usuniete']
        self.parts = backParam['parts']
        self.e = backParam['e']
        self.send_count = backParam['send_count']
        for i in message_new:
            self.message[i] = message_new[i]
        text = ""
        length = len(data)/n
        for i in range(0,n):
            if i in self.message.keys():
                text+= self.message[i]
            else:
                text+= length * 'X'
        text += '\nZdekodowano za pomoca kodu LT,\n wyslano %s pakietow,\nodebrano %s pakietow,\nusunieto %s pakietow' % (self.send_count, self.send_count - self.e, self.e)
        self.ui.textDecode.setText(text)
        val = round(len(self.message)*100/n)
        self.ui.progressBar.setValue(val)
        self.ui.encodeButton.setEnabled(False)
        self.ui.resendButton.setEnabled(True)
    def generateHist(self):
        self.clearData()
        histData = []
        data = str(self.ui.textEncode.toPlainText())
        n = self.ui.packetLength.value()
        c = self.ui.cParam.value()
        delta = self.ui.deltaParam.value()
        pBEC = self.ui.becParam.value()
        probeLength = self.ui.probeLength.value()
        for i in range(probeLength):
            m = multiply(self.distType)
            backParam = {'message': {}, 'parts': {}, 'send_count': 0, 'e': 0, 'multiply': m}
            odpowiedz = decodeMessage(data,n, pBEC, self.distType, c, delta, backParam)
            histData.append(odpowiedz['ilosc'])
            val = round(len(histData)*100/probeLength)
            self.ui.progressBar.setValue(val)
            
        print histData
        showHist(histData, 50)
    def resendData(self):
        self.multiply = multiply(self.distType)
        self.flowData()
        tekst = str(self.ui.textEncode.toPlainText())
        tekst += '\nZdekodowano za pomoca kodu LT,\n wyslano %s pakietow,\nodebrano %s pakietow,\nusunieto %s pakietow' % (self.send_count, self.send_count - self.e, self.e)
        self.ui.textDecode.setText(tekst)
        self.ui.progressBar.setValue(100)
    def selectFile(self):
        self.path = QtGui.QFileDialog.getOpenFileName(self, 'Open File', '.')
        if self.path != "":
            self.distType = self.path
        else:
            self.distType = False
    def rsdParam(self):
        self.ui.deltaParam.setEnabled(True)
        self.ui.cParam.setEnabled(True)
        self.ui.inputFileButton.setEnabled(False)
        self.distType = 'r'
    def isdParam(self):
        self.ui.deltaParam.setEnabled(False)
        self.ui.cParam.setEnabled(False)
        self.ui.inputFileButton.setEnabled(False)
        self.distType = 'i'
    def uddParam(self):
        self.ui.deltaParam.setEnabled(False)
        self.ui.cParam.setEnabled(False)
        self.ui.inputFileButton.setEnabled(True)
    def clearData(self):
        self.message = {}
        self.parts = {}
        self.send_count = 0
        self.e = 0
        self.multiply = 1.0
        self.ui.textDecode.clear()
        self.ui.progressBar.setValue(0)
        self.ui.encodeButton.setEnabled(True)
        self.ui.resendButton.setEnabled(False)
    
class rlfDialog(QtGui.QDialog):
    message = {}
    parts = {}
    send_count = 0
    e = 0
    multiply = 1.0
    def __init__(self, parent=None):
        super(rlfDialog, self).__init__(parent)
        self.ui = Ui_Random_Linear_Fountain_Code()
        self.ui.setupUi(self)
        self.ui.progressBar.setValue(0)
        self.ui.resendButton.setEnabled(False)
        QtCore.QObject.connect(self.ui.encodeButton,QtCore.SIGNAL("clicked()"), self.flowData)
        QtCore.QObject.connect(self.ui.resendButton,QtCore.SIGNAL("clicked()"), self.resendData)
        QtCore.QObject.connect(self.ui.clearButton,QtCore.SIGNAL("clicked()"), self.clearData)
        QtCore.QObject.connect(self.ui.histButton,QtCore.SIGNAL("clicked()"), self.generateHist)
    def flowData(self):
        data = str(self.ui.textEncode.toPlainText())
        n = self.ui.packetLength.value()
        pBEC = self.ui.becParam.value()
        distType = 'm'
        c = 0
        delta = 0
        backParam = {'message': self.message, 'parts': self.parts, 'send_count': self.send_count, 'e': self.e, 'multiply': self.multiply}
        odpowiedz = decodeMessage(data,n, pBEC, distType, c, delta, backParam)
        backParam = odpowiedz['backParam']
        message_new = odpowiedz['wiadomosc']
        recived_num = odpowiedz['ilosc']
        erased_num = odpowiedz['usuniete']
        self.parts = backParam['parts']
        self.e = backParam['e']
        self.send_count = backParam['send_count']
        for i in message_new:
            self.message[i] = message_new[i]
        text = ""
        length = len(data)/n
        for i in range(0,n):
            if i in self.message.keys():
                text+= self.message[i]
            else:
                text+= length * 'X'
        text += '\nZdekodowano za pomoca kodu RLF,\n wyslano %s pakietow,\nodebrano %s pakietow,\nusunieto %s pakietow' % (self.send_count, self.send_count - self.e, self.e)
        self.ui.textDecode.setText(text)
        val = round(len(self.message)*100/n)
        self.ui.progressBar.setValue(val)
        self.ui.encodeButton.setEnabled(False)
        self.ui.resendButton.setEnabled(True)
    def generateHist(self):
        self.clearData()
        histData = []
        data = str(self.ui.textEncode.toPlainText())
        n = self.ui.packetLength.value()
        c = 0
        delta = 0
        pBEC = self.ui.becParam.value()
        probeLength = self.ui.probeLength.value()
        for i in range(probeLength):
            backParam = {'message': {}, 'parts': {}, 'send_count': 0, 'e': 0, 'multiply': 100}
            odpowiedz = decodeMessage(data,n, pBEC, 'm', c, delta, backParam)
            histData.append(odpowiedz['ilosc'])
            val = round(len(histData)*100/probeLength)
            self.ui.progressBar.setValue(val)
            
        print histData
        showHist(histData, 50)
    def resendData(self):
        self.multiply+=0.5
        self.flowData()
    def clearData(self):
        self.message = {}
        self.parts = {}
        self.send_count = 0
        self.e = 0
        self.multiply = 1.0
        self.ui.textDecode.clear()
        self.ui.progressBar.setValue(0)
        self.ui.encodeButton.setEnabled(True)
        self.ui.resendButton.setEnabled(False)
    
#file = open('strumien','wb')
#state = random.getstate()
#file.write(encode("osiemlit",4))
#file.close()

#file = open('strumien','rb')
#random.setstate(state)
#data = file.read()
#print decode(data, 4)
#file.close()
data = 'bardzo dlugi tekst do przeslania przez kanal z wymazywaniem po to aby sprawdzic skutecznosc kodowania luby transform oraz random linear fountain code.'
#data = 'ala ma kota'
n = 15
pBEC = 0.001
distType = 'i'
c = 0.03
delta = 0.01
#decodeMessage(data,n, pBEC, distType, c, delta)
#print len("bardzo dlugi tekst do przeslania przez kanal z wymazywaniem po to aby sprawdzic skutecznosc kodowania luby transform oraz random linear fountain code.")
"""set_er = set()
random_list = range(1,100)
set_er = random.sample(random_list, 5)
#print set_er
random.setstate(state)
set_er2 = set()
set_er2 = random.sample(random_list, 5)
print set_er, set_er2"""

if __name__ == "__main__":
    import sys

    app = QtGui.QApplication(sys.argv)
    app.setApplicationName('MyDialog')

    main = MyDialog()
    main.show()

    sys.exit(app.exec_())
import ast

def generateWorkingFile(path):
    fname = open(path,'w')
    mydict = {}
    for i in range(10):
        mydict[i] = i+1
    data = str(mydict)
    fname.write(data)
    fname.close()

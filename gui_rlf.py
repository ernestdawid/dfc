# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'gui_rlf.ui'
#
# Created: Tue Jul 01 13:49:26 2014
#      by: PyQt4 UI code generator 4.11
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Random_Linear_Fountain_Code(object):
    def setupUi(self, Random_Linear_Fountain_Code):
        Random_Linear_Fountain_Code.setObjectName(_fromUtf8("Random_Linear_Fountain_Code"))
        Random_Linear_Fountain_Code.resize(564, 421)
        self.label = QtGui.QLabel(Random_Linear_Fountain_Code)
        self.label.setGeometry(QtCore.QRect(20, 30, 141, 16))
        self.label.setObjectName(_fromUtf8("label"))
        self.textDecode = QtGui.QTextBrowser(Random_Linear_Fountain_Code)
        self.textDecode.setGeometry(QtCore.QRect(20, 240, 251, 161))
        self.textDecode.setObjectName(_fromUtf8("textDecode"))
        self.label_2 = QtGui.QLabel(Random_Linear_Fountain_Code)
        self.label_2.setGeometry(QtCore.QRect(20, 220, 141, 16))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label_7 = QtGui.QLabel(Random_Linear_Fountain_Code)
        self.label_7.setGeometry(QtCore.QRect(520, 50, 31, 16))
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.becParam = QtGui.QDoubleSpinBox(Random_Linear_Fountain_Code)
        self.becParam.setEnabled(True)
        self.becParam.setGeometry(QtCore.QRect(430, 50, 71, 22))
        self.becParam.setReadOnly(False)
        self.becParam.setDecimals(4)
        self.becParam.setMaximum(1.0)
        self.becParam.setSingleStep(0.01)
        self.becParam.setProperty("value", 0.01)
        self.becParam.setObjectName(_fromUtf8("becParam"))
        self.packetLength = QtGui.QSpinBox(Random_Linear_Fountain_Code)
        self.packetLength.setGeometry(QtCore.QRect(290, 50, 61, 22))
        self.packetLength.setMaximum(1000000)
        self.packetLength.setProperty("value", 10)
        self.packetLength.setObjectName(_fromUtf8("packetLength"))
        self.label_4 = QtGui.QLabel(Random_Linear_Fountain_Code)
        self.label_4.setGeometry(QtCore.QRect(360, 50, 61, 16))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.clearButton = QtGui.QPushButton(Random_Linear_Fountain_Code)
        self.clearButton.setGeometry(QtCore.QRect(330, 280, 111, 23))
        self.clearButton.setObjectName(_fromUtf8("clearButton"))
        self.encodeButton = QtGui.QPushButton(Random_Linear_Fountain_Code)
        self.encodeButton.setGeometry(QtCore.QRect(330, 190, 111, 23))
        self.encodeButton.setObjectName(_fromUtf8("encodeButton"))
        self.resendButton = QtGui.QPushButton(Random_Linear_Fountain_Code)
        self.resendButton.setGeometry(QtCore.QRect(330, 240, 111, 23))
        self.resendButton.setObjectName(_fromUtf8("resendButton"))
        self.progressBar = QtGui.QProgressBar(Random_Linear_Fountain_Code)
        self.progressBar.setGeometry(QtCore.QRect(300, 380, 251, 23))
        self.progressBar.setProperty("value", 0)
        self.progressBar.setObjectName(_fromUtf8("progressBar"))
        self.histButton = QtGui.QPushButton(Random_Linear_Fountain_Code)
        self.histButton.setGeometry(QtCore.QRect(330, 320, 111, 23))
        self.histButton.setObjectName(_fromUtf8("histButton"))
        self.label_8 = QtGui.QLabel(Random_Linear_Fountain_Code)
        self.label_8.setGeometry(QtCore.QRect(520, 320, 51, 16))
        self.label_8.setObjectName(_fromUtf8("label_8"))
        self.probeLength = QtGui.QSpinBox(Random_Linear_Fountain_Code)
        self.probeLength.setGeometry(QtCore.QRect(450, 320, 61, 22))
        self.probeLength.setMaximum(10000)
        self.probeLength.setProperty("value", 100)
        self.probeLength.setObjectName(_fromUtf8("probeLength"))
        self.textEncode = QtGui.QTextEdit(Random_Linear_Fountain_Code)
        self.textEncode.setGeometry(QtCore.QRect(20, 50, 251, 161))
        self.textEncode.setObjectName(_fromUtf8("textEncode"))

        self.retranslateUi(Random_Linear_Fountain_Code)
        QtCore.QMetaObject.connectSlotsByName(Random_Linear_Fountain_Code)

    def retranslateUi(self, Random_Linear_Fountain_Code):
        Random_Linear_Fountain_Code.setWindowTitle(_translate("Random_Linear_Fountain_Code", "Form", None))
        self.label.setText(_translate("Random_Linear_Fountain_Code", "Tekst do zakodowania", None))
        self.textDecode.setHtml(_translate("Random_Linear_Fountain_Code", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"><br /></p></body></html>", None))
        self.label_2.setText(_translate("Random_Linear_Fountain_Code", "Zdekodowany tekst", None))
        self.label_7.setText(_translate("Random_Linear_Fountain_Code", "BEC", None))
        self.label_4.setText(_translate("Random_Linear_Fountain_Code", "ilość", None))
        self.clearButton.setText(_translate("Random_Linear_Fountain_Code", "wyczyść dane", None))
        self.encodeButton.setText(_translate("Random_Linear_Fountain_Code", "zakoduj", None))
        self.resendButton.setText(_translate("Random_Linear_Fountain_Code", "doślij pakiety", None))
        self.histButton.setText(_translate("Random_Linear_Fountain_Code", "generuj wykres", None))
        self.label_8.setText(_translate("Random_Linear_Fountain_Code", "próbek", None))
        self.textEncode.setHtml(_translate("Random_Linear_Fountain_Code", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">bardzo dlugi tekst do przeslania przez kanal z wymazywaniem po to aby sprawdzic skutecznosc kodowania luby transform oraz random linear fountain code.</span></p></body></html>", None))

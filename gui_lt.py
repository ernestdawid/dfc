# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'gui_lt.ui'
#
# Created: Tue Jul 01 13:49:29 2014
#      by: PyQt4 UI code generator 4.11
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Luby_Transform(object):
    def setupUi(self, Luby_Transform):
        Luby_Transform.setObjectName(_fromUtf8("Luby_Transform"))
        Luby_Transform.resize(566, 421)
        self.encodeButton = QtGui.QPushButton(Luby_Transform)
        self.encodeButton.setGeometry(QtCore.QRect(320, 180, 111, 23))
        self.encodeButton.setObjectName(_fromUtf8("encodeButton"))
        self.resendButton = QtGui.QPushButton(Luby_Transform)
        self.resendButton.setGeometry(QtCore.QRect(320, 240, 111, 23))
        self.resendButton.setObjectName(_fromUtf8("resendButton"))
        self.textDecode = QtGui.QTextBrowser(Luby_Transform)
        self.textDecode.setGeometry(QtCore.QRect(20, 240, 251, 161))
        self.textDecode.setObjectName(_fromUtf8("textDecode"))
        self.label = QtGui.QLabel(Luby_Transform)
        self.label.setGeometry(QtCore.QRect(20, 30, 151, 16))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_2 = QtGui.QLabel(Luby_Transform)
        self.label_2.setGeometry(QtCore.QRect(20, 220, 131, 16))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.inputFileButton = QtGui.QPushButton(Luby_Transform)
        self.inputFileButton.setGeometry(QtCore.QRect(440, 110, 101, 23))
        self.inputFileButton.setObjectName(_fromUtf8("inputFileButton"))
        self.isdRadioButton = QtGui.QRadioButton(Luby_Transform)
        self.isdRadioButton.setGeometry(QtCore.QRect(360, 80, 61, 17))
        self.isdRadioButton.setChecked(True)
        self.isdRadioButton.setObjectName(_fromUtf8("isdRadioButton"))
        self.rsdRadioButton = QtGui.QRadioButton(Luby_Transform)
        self.rsdRadioButton.setGeometry(QtCore.QRect(290, 80, 61, 17))
        self.rsdRadioButton.setObjectName(_fromUtf8("rsdRadioButton"))
        self.userDistRadioButton = QtGui.QRadioButton(Luby_Transform)
        self.userDistRadioButton.setGeometry(QtCore.QRect(440, 80, 111, 17))
        self.userDistRadioButton.setObjectName(_fromUtf8("userDistRadioButton"))
        self.packetLength = QtGui.QSpinBox(Luby_Transform)
        self.packetLength.setGeometry(QtCore.QRect(290, 50, 61, 22))
        self.packetLength.setMaximum(1000000)
        self.packetLength.setProperty("value", 10)
        self.packetLength.setObjectName(_fromUtf8("packetLength"))
        self.label_3 = QtGui.QLabel(Luby_Transform)
        self.label_3.setGeometry(QtCore.QRect(360, 50, 61, 16))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.deltaParam = QtGui.QDoubleSpinBox(Luby_Transform)
        self.deltaParam.setEnabled(False)
        self.deltaParam.setGeometry(QtCore.QRect(290, 110, 71, 22))
        self.deltaParam.setReadOnly(False)
        self.deltaParam.setDecimals(4)
        self.deltaParam.setMaximum(1.0)
        self.deltaParam.setSingleStep(0.01)
        self.deltaParam.setProperty("value", 0.1)
        self.deltaParam.setObjectName(_fromUtf8("deltaParam"))
        self.label_4 = QtGui.QLabel(Luby_Transform)
        self.label_4.setGeometry(QtCore.QRect(380, 110, 31, 16))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.label_5 = QtGui.QLabel(Luby_Transform)
        self.label_5.setGeometry(QtCore.QRect(380, 140, 31, 16))
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.cParam = QtGui.QDoubleSpinBox(Luby_Transform)
        self.cParam.setEnabled(False)
        self.cParam.setGeometry(QtCore.QRect(290, 140, 71, 22))
        self.cParam.setReadOnly(False)
        self.cParam.setDecimals(4)
        self.cParam.setMaximum(1.0)
        self.cParam.setSingleStep(0.01)
        self.cParam.setProperty("value", 0.03)
        self.cParam.setObjectName(_fromUtf8("cParam"))
        self.textEncode = QtGui.QTextEdit(Luby_Transform)
        self.textEncode.setGeometry(QtCore.QRect(20, 50, 251, 161))
        self.textEncode.setObjectName(_fromUtf8("textEncode"))
        self.label_6 = QtGui.QLabel(Luby_Transform)
        self.label_6.setGeometry(QtCore.QRect(520, 50, 31, 16))
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.becParam = QtGui.QDoubleSpinBox(Luby_Transform)
        self.becParam.setEnabled(True)
        self.becParam.setGeometry(QtCore.QRect(430, 50, 71, 22))
        self.becParam.setReadOnly(False)
        self.becParam.setDecimals(4)
        self.becParam.setMaximum(1.0)
        self.becParam.setSingleStep(0.01)
        self.becParam.setProperty("value", 0.01)
        self.becParam.setObjectName(_fromUtf8("becParam"))
        self.progressBar = QtGui.QProgressBar(Luby_Transform)
        self.progressBar.setGeometry(QtCore.QRect(290, 370, 251, 23))
        self.progressBar.setProperty("value", 0)
        self.progressBar.setObjectName(_fromUtf8("progressBar"))
        self.clearButton = QtGui.QPushButton(Luby_Transform)
        self.clearButton.setGeometry(QtCore.QRect(320, 280, 111, 23))
        self.clearButton.setObjectName(_fromUtf8("clearButton"))
        self.histButton = QtGui.QPushButton(Luby_Transform)
        self.histButton.setGeometry(QtCore.QRect(320, 320, 111, 23))
        self.histButton.setObjectName(_fromUtf8("histButton"))
        self.label_7 = QtGui.QLabel(Luby_Transform)
        self.label_7.setGeometry(QtCore.QRect(510, 320, 51, 16))
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.probeLength = QtGui.QSpinBox(Luby_Transform)
        self.probeLength.setGeometry(QtCore.QRect(440, 320, 61, 22))
        self.probeLength.setMaximum(10000)
        self.probeLength.setProperty("value", 100)
        self.probeLength.setObjectName(_fromUtf8("probeLength"))

        self.retranslateUi(Luby_Transform)
        QtCore.QMetaObject.connectSlotsByName(Luby_Transform)

    def retranslateUi(self, Luby_Transform):
        Luby_Transform.setWindowTitle(_translate("Luby_Transform", "Form", None))
        self.encodeButton.setText(_translate("Luby_Transform", "zakoduj", None))
        self.resendButton.setText(_translate("Luby_Transform", "doślij pakiety", None))
        self.textDecode.setHtml(_translate("Luby_Transform", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"><br /></p></body></html>", None))
        self.label.setText(_translate("Luby_Transform", "Tekst do zakodowania", None))
        self.label_2.setText(_translate("Luby_Transform", "Zdekodowany tekst", None))
        self.inputFileButton.setText(_translate("Luby_Transform", "wprowadź plik", None))
        self.isdRadioButton.setText(_translate("Luby_Transform", "ISD", None))
        self.rsdRadioButton.setText(_translate("Luby_Transform", "RSD", None))
        self.userDistRadioButton.setText(_translate("Luby_Transform", "własny rozkład", None))
        self.label_3.setText(_translate("Luby_Transform", "ilość", None))
        self.label_4.setText(_translate("Luby_Transform", "delta", None))
        self.label_5.setText(_translate("Luby_Transform", "c", None))
        self.textEncode.setHtml(_translate("Luby_Transform", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">bardzo dlugi tekst do przeslania przez kanal z wymazywaniem po to aby sprawdzic skutecznosc kodowania luby transform oraz random linear fountain code.</span></p></body></html>", None))
        self.label_6.setText(_translate("Luby_Transform", "BEC", None))
        self.clearButton.setText(_translate("Luby_Transform", "wyczyść dane", None))
        self.histButton.setText(_translate("Luby_Transform", "generuj wykres", None))
        self.label_7.setText(_translate("Luby_Transform", "próbek", None))
